const workboxPlugin = require('workbox-webpack-plugin');
const path = require('path');
const fs = require('fs');

/*
* Removes the SWPrecache plugin if exists.
*/
const removeSWPrecachePlugin = (config) => {
  const swPrecachePluginIndex = config.plugins.findIndex((element) => {
    const result = element.constructor.name === 'SWPrecacheWebpackPlugin';
    return result;
  });

  if (swPrecachePluginIndex !== -1) {
    config.plugins.splice(swPrecachePluginIndex, 1);
  }

  return config;
};

/*
* Export all environment varaibles that starts
* with REACT_APP to config.js inside public folder.
* Used with custom service workers, where service
* workers requires environment variables.
*/
const compileEnvironmentVariables = () => {
  const fileContent = Object.keys(process.env)
    .filter((key) => key.startsWith('REACT_APP'))
    .reduce((accumulator, key) => {
      let str = accumulator;

      const value = process.env[key];
      str += `const ${key.substr(10, key.length)} = '${value}';\n`;
      return str;
    }, '');

  fs.writeFileSync('./public/config.js', fileContent);
};

module.exports = {
  webpack: (cfg) => {
    const workboxConfigProd = {
      swSrc: path.join(__dirname, 'public', 'sw.js'),
      swDest: 'sw.js',
      importWorkboxFrom: 'disabled',
    };

    compileEnvironmentVariables();

    const config = removeSWPrecachePlugin(cfg);
    config.plugins.push(new workboxPlugin.InjectManifest(workboxConfigProd));

    return config;
  },
};
