importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');
importScripts('./config.js');

// Use workbox network first strategy.
workbox.routing.registerRoute(HOST, 
  new workbox.strategies.NetworkFirst());

workbox.routing.registerRoute(/\.(?:js|css|html|png|jpeg|jpg)$/, 
  new workbox.strategies.NetworkFirst());
